'use strict';

var expect = require('chai').expect;
var config = require(__dirname + '/../src/config');

describe('config', function(){

	it('should contain configuration for development, testing and production', function(){
		expect(config).to.be.an('object');
		expect(config).to.have.keys(['development', 'testing']);
		expect(config.development).to.be.an('object');
		expect(config.testing).to.be.an('object');
	});

	describe('development', function(){

		it('should contain port configuration', function(){
			expect(config.development).to.have.property('webport');
			expect(config.development.webport).to.be.equal(4000);
		});

		it('should contain ses configuration', function(){
			expect(config.development).to.have.property('ses');
			expect(config.development.ses).to.be.an('object');
			expect(config.development.ses).to.have.keys(['key', 'secret', 'sender']);
			expect(config.development.ses.key).to.be.equal('Demo');
			expect(config.development.ses.secret).to.be.equal('Demo');
			expect(config.development.ses.sender).to.be.equal('Ahmed Hashem <ahashem302@gmail.com>');
		});

		it('should contain sqs configuration', function(){
			expect(config.development).to.have.property('sqs');
			expect(config.development.sqs).to.be.an('object');
			expect(config.development.sqs).to.have.keys(['accessKey', 'accessToken', 'queue']);
			expect(config.development.sqs.accessKey).to.be.equal('Demo');
			expect(config.development.sqs.accessToken).to.be.equal('Demo');
			expect(config.development.sqs.queue).to.be.equal('Demo');
		});

		it('should contain log entries configuration', function(){
			expect(config.development).to.have.property('logEntries');
			expect(config.development.webport).to.be.equal('7dd74b90-d82f-40b5-a36b-3835cba2c46e');
		});
		
	});

	describe('testing', function(){

		it('should contain port configuration', function(){
			expect(config.testing).to.have.property('webport');
			expect(config.testing.webport).to.be.equal(4100);
		});

		it('should contain ses configuration', function(){
			expect(config.testing).to.have.property('ses');
			expect(config.testing.ses).to.be.an('object');
			expect(config.testing.ses).to.have.keys(['key', 'secret', 'sender']);
			expect(config.testing.ses.key).to.be.equal('AKIAINYYB7R63YD3TS6A');
			expect(config.testing.ses.secret).to.be.equal('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');
			expect(config.testing.ses.sender).to.be.equal('Ahmed Hashem <ahashem302@gmail.com>');
		});

		it('should contain sqs configuration', function(){
			expect(config.testing).to.have.property('sqs');
			expect(config.testing.sqs).to.be.an('object');
			expect(config.testing.sqs).to.have.keys(['accessKey', 'accessToken', 'queue']);
			expect(config.testing.sqs.accessKey).to.be.equal('Demo');
			expect(config.testing.sqs.accessToken).to.be.equal('Demo');
			expect(config.testing.sqs.queue).to.be.equal('Demo');
		});

		it('should contain log entries configuration', function(){
			expect(config.development).to.have.property('logEntries');
			expect(config.development.webport).to.be.equal('7dd74b90-d82f-40b5-a36b-3835cba2c46e');
		});
		
	});

});
