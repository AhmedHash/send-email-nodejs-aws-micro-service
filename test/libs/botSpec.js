'use strict';

var expect = require('chai').expect;
var SQS = require('aws-sqs-helper');
var is = require('is_js');
var config = require(__dirname + '/../../src/config').testing;
var SES = require(__dirname + '/../../src/libs/ses');
var Bot = require(__dirname + '/../../src/libs/bot.js');

describe('bot', function(){

	describe('init', function(){

		it('should throw an error if no config', function(){
			var fn = function(){ new Bot(); };
			expect(fn).to.throw(/not_config/);
		});

		it('should throw an error if config isn\'t an object', function(){
			var fn = function(){ new Bot('config'); };
			expect(fn).to.throw(/invalid_config/);
		});

	});

	describe('run', function(){

		it('should return no data found', function(done){
			this.timeout(10000);
			var sqs = new SQS(config.sqs);
			var bot = new Bot(config);
			sqs.receiveMessage(function (err, message){
				if(is.not.empty(message))
					sqs.deleteMessage(message[0].ReceiptHandle, function(err, data){});
				bot.run(function(err, data, next_run){
					expect(err).to.be.null;
					expect(data).to.have.a.property('status');
					expect(data.status).to.be.equal('no data found');
					expect(next_run).to.be.equal(10000);
					done();
				});
			});
		});

		it('should return email not sent', function(done){
			this.timeout(10000);
			var bot = new Bot(config);
			var sqs = new SQS(config.sqs);
			var data = {
				sender: '',
				to: 'b@crowdanalyzer.com',
				subject: 'Hello from SES',
				text: 'Test Email',
				html: '<b>Test Email</b>'
			};
			sqs.sendMessage(data, function(err, data){
				bot.run(function(err, data, next_run){
					expect(err).to.not.be.null;
					expect(err).to.have.a.property('status');
					expect(err.status).to.be.equal('email not sent');
					expect(next_run).to.be.equal(1000);
					done();
				});
			});
		});

		it('should return email sent', function(done){
			this.timeout(10000);
			var bot = new Bot(config);
			var sqs = new SQS(config.sqs);
			var data = {
				sender: 'Crowd Analyzer <dev@crowdanalyzer.com>',
				to: 'b@crowdanalyzer.com',
				subject: 'Hello from SES',
				text: 'Test Email',
				html: '<b>Test Email</b>'
			};
			sqs.sendMessage(data, function(err, data){
				bot.run(function(err, data, next_run){
					expect(err).to.be.null;
					expect(data).to.have.a.property('status');
					expect(data.status).to.be.equal('email sent');
					expect(next_run).to.be.equal(1000);
					done();
				});
			});
		});

	});

});