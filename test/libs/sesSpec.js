'use strict';

var expect = require('chai').expect;
var SES = require(__dirname + '/../../src/libs/ses');
var Base = require(__dirname + '/../../src/base');
var config = require(__dirname + '/../../src/config').testing;

describe('SES', function(){

	describe('init', function(){

		it('should throw an error if no config', function(){
			var fn = function(){ new SES(); };
			expect(fn).to.throw(/not_config/);
		});

		it('should throw an error if config isn\'t an object', function(){
			var fn = function(){ new SES('config'); };
			expect(fn).to.throw(/invalid_config/);
		});

		it('should throw an error if no key', function(){
			var fn = function(){ new SES({}); };
			expect(fn).to.throw(/not_key/);
		});

		it('should throw an error if key isn\'t a string', function(){
			var fn = function(){ new SES({key: []}); };
			expect(fn).to.throw(/invalid_key/);
		});

		it('should throw an error if no secret', function(){
			var fn = function(){ new SES({
				key: 'key'
			}); };
			expect(fn).to.throw(/not_secret/);
		});

		it('should throw an error if secret isn\'t a string', function(){
			var fn = function(){ new SES({
				key: 'key',
				secret: []
			}); };
			expect(fn).to.throw(/invalid_secret/);
		});

		it('should init ses client and keep record of sender', function(){
			var ses = new SES(config.ses);
			expect(ses).to.have.property('client');
		});

		it('should extend base', function(){
			var ses = new SES(config.ses);
			expect(ses).to.be.instanceof(Base);
		});
	});

	describe('send', function(){

		it('should throw an error if no callback', function(){
			var fn = function(){
				var ses = new SES(config.ses);
				ses.send();
			};
			expect(fn).to.throw(/not_callback/);
		});

		it('should throw an error if callback isn\'t a function', function(){
			var fn = function(){
				var ses = new SES(config.ses);
				ses.send('from', 'to', 'subject', 'text', 'html', 'callback');
			};
			expect(fn).to.throw(/invalid_callback/);
		});

		it('should return an error if no sender', function(done){
			var ses = new SES(config.ses);
			ses.send(function(err, messageId){
				expect(err).to.not.be.null;
				expect(err).to.be.equal('not_sender');
				expect(messageId).to.be.undefined;
				done();
			});
		});

		it('should return an error if sender isn\'t a string', function(done){
			var ses = new SES(config.ses);
			ses.send([], function(err, messageId){
				expect(err).to.not.be.null;
				expect(err).to.be.equal('invalid_sender');
				expect(messageId).to.be.undefined;
				done();
			});
		});


		it('should return an error if no to', function(done){
			var ses = new SES(config.ses);
			ses.send('from', function(err, messageId){
				expect(err).to.not.be.null;
				expect(err).to.be.equal('not_to');
				expect(messageId).to.be.undefined;
				done();
			});
		});

		it('should return an error if to isn\'t a string', function(done){
			var ses = new SES(config.ses);
			ses.send('from', [], function(err, messageId){
				expect(err).to.not.be.null;
				expect(err).to.be.equal('invalid_to');
				expect(messageId).to.be.undefined;
				done();
			});
		});

		it('should return an error if no subject', function(done){
			var ses = new SES(config.ses);
			ses.send('from', 'to', function(err, messageId){
				expect(err).to.not.be.null;
				expect(err).to.be.equal('not_subject');
				expect(messageId).to.be.undefined;
				done();
			});
		});

		it('should return an error if subject isn\'t a string', function(done){
			var ses = new SES(config.ses);
			ses.send('from', 'to', [], function(err, messageId){
				expect(err).to.not.be.null;
				expect(err).to.be.equal('invalid_subject');
				expect(messageId).to.be.undefined;
				done();
			});
		});

		it('should return an error if no text message', function(done){
			var ses = new SES(config.ses);
			ses.send('from', 'to', 'subject', function(err, messageId){
				expect(err).to.not.be.null;
				expect(err).to.be.equal('not_text');
				expect(messageId).to.be.undefined;
				done();
			});
		});

		it('should return an error if text message isn\'t a string', function(done){
			var ses = new SES(config.ses);
			ses.send('from', 'to', 'subject', [], function(err, messageId){
				expect(err).to.not.be.null;
				expect(err).to.be.equal('invalid_text');
				expect(messageId).to.be.undefined;
				done();
			});
		});

		it('should return an error if no html message', function(done){
			var ses = new SES(config.ses);
			ses.send('from', 'to', 'subject', 'text', function(err, messageId){
				expect(err).to.not.be.null;
				expect(err).to.be.equal('not_html');
				expect(messageId).to.be.undefined;
				done();
			});
		});

		it('should return an error if html message isn\'t a string', function(done){
			var ses = new SES(config.ses);
			ses.send('from', 'to', 'subject', 'text', [], function(err, messageId){
				expect(err).to.not.be.null;
				expect(err).to.be.equal('invalid_html');
				expect(messageId).to.be.undefined;
				done();
			});
		});

		it('should return an error from ses api', function(done){
			this.timeout(10000);
			var ses = new SES({
				key: 'key',
				secret: 'secret'
			});
			ses.send('from', 'to', 'subject', 'text', 'html', function(err, messageId){
				expect(err).to.be.equal('InvalidClientTokenId: The security token included in the request is invalid.');
				expect(messageId).to.be.undefined;
				done();
			});
		});

		it('should send the email', function(done){
			this.timeout(10000);
			var ses = new SES(config.ses);
			ses.send('Crowd Analyzer <dev@crowdanalyzer.com>', 'b@crowdanalyzer.com', 'Hello from SES', 'Test Email', '<b>Test Email</b>', function(err, messageId){
				expect(err).to.be.null;
				expect(messageId).to.be.a('string');
				done();
			});
		});
	});
});
