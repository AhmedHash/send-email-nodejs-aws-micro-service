'use strict';

var expect = require('chai').expect;
var Base = require(__dirname + '/../src/base');

describe('Base', function(){

	describe('validateCallback', function(){

		it('should throw an error if no callback', function(){
			var fn = function(){
				var base = new Base();
				base.validateCallback();
			};
			expect(fn).to.throw(/not_callback/);
		});

		it('should throw an error if callback isn\'t a function', function(){
			var fn = function(){
				var base = new Base();
				base.validateCallback('callback');
			};
			expect(fn).to.throw(/invalid_callback/);
		});
	});
});
