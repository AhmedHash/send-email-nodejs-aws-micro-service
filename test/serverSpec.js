'use strict';

var expect = require('chai').expect;
var SQS = require('aws-sqs-helper');
var is = require('is_js');
var Logger = require('le_node');
var config = require(__dirname + '/../src/config').testing;
var SES = require(__dirname + '/../src/libs/ses');
var Base = require(__dirname + '/../src/server.js');

describe('server', function(){

	describe('SES', function(){
		it('should throw an error if no config', function(){
			var fn = function(){ new SES(); };
			expect(fn).to.throw(/not_config/);
		});
		it('should throw an error if config isn\'t an object', function(){
			var fn = function(){ new SES('config'); };
			expect(fn).to.throw(/invalid_config/);
		});

		it('should throw an error if no key', function(){
			var fn = function(){ new SES({}); };
			expect(fn).to.throw(/not_key/);
		});

		it('should throw an error if key isn\'t a string', function(){
			var fn = function(){ new SES({key: []}); };
			expect(fn).to.throw(/invalid_key/);
		});

		it('should throw an error if no secret', function(){
			var fn = function(){ new SES({
				key: 'key'
			}); };
			expect(fn).to.throw(/not_secret/);
		});

		it('should throw an error if secret isn\'t a string', function(){
			var fn = function(){ new SES({
				key: 'key',
				secret: []
			}); };
			expect(fn).to.throw(/invalid_secret/);
		});
	});

	describe('SQS', function(){
		it('should throw an error if no config', function(){
			var fn = function(){ new SQS(); };
			expect(fn).to.throw(/SQS configuration is required./);
		});

		it('should throw an error if config isn\'t an object', function(){
			var fn = function(){ new SQS('config'); };
			expect(fn).to.throw(/SQS configuration should be an object./);
		});

		it('should throw an error if no access key', function(){
			var fn = function(){ new SQS({}); };
			expect(fn).to.throw(/SQS access key is required./);
		});

		it('should throw an error if access key isn\'t a string', function(){
			var fn = function(){ new SQS({accessKey: []}); };
			expect(fn).to.throw(/SQS access key should be a string./);
		});

		it('should throw an error if no access token', function(){
			var fn = function(){ new SQS({
				accessKey: 'key'
			}); };
			expect(fn).to.throw(/SQS access token is required./);
		});

		it('should throw an error if access token isn\'t a string', function(){
			var fn = function(){ new SQS({
				accessKey: 'key',
				accessToken: []
			}); };
			expect(fn).to.throw(/SQS access token should be a string./);
		});

		it('should throw an error if no queue', function(){
			var fn = function(){ new SQS({
				accessKey: 'key',
				accessToken: 'accessToken'
			}); };
			expect(fn).to.throw(/SQS queue url is required./);
		});

		it('should throw an error if queue isn\'t a string', function(){
			var fn = function(){ new SQS({
				accessKey: 'key',
				accessToken: 'accessToken',
				queue: []
			}); };
			expect(fn).to.throw(/SQS queue url should be a string./);
		});
	});

	describe('Log entries', function(){
		it('should throw an error if no config', function(){
			var fn = function(){ new Logger(); };
			expect(fn).to.throw(/Cannot read property \'bufferSize\' of undefined/);
		});
	});

});