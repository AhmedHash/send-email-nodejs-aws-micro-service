'use strict';

var AWS = require('aws-sdk');
var Base = require(__dirname + '/../base');
var is = require('is_js');

function SES(config) {
	if(!config)
		throw new Error('not_config');

	if(is.not.json(config))
		throw new Error('invalid_config');

	if(!config.key)
		throw new Error('not_key');

	if(is.not.string(config.key))
		throw new Error('invalid_key');

	if(!config.secret)
		throw new Error('not_secret');

	if(is.not.string(config.secret))
		throw new Error('invalid_secret');

	this.client = new AWS.SES({
		accessKeyId: config.key,
		secretAccessKey: config.secret,
		region: 'us-east-1'
	});
	
	Base.call(this);
}

SES.prototype = Object.create(Base.prototype);

SES.prototype.send = function(sender, to, subject, text, html, callback){
	if(!callback)
	{

		if(is.function(html))
		{
			callback = html;
			html = null;
		}
		else if(is.function(text))
		{
			callback = text;
			text = null;
		}
		else if(is.function(subject))
		{
			callback = subject;
			subject = null;
		}
		else if(is.function(to))
		{
			callback = to;
			to = null;
		}
		else if(is.function(sender))
		{
			callback = sender;
			sender = null;
		}
	}

	this.validateCallback(callback);

	if(!sender)
		return callback('not_sender');

	if(is.not.string(sender))
		return callback('invalid_sender');

	if(!to)
		return callback('not_to');

	if(is.not.string(to))
		return callback('invalid_to');

	if(!subject)
		return callback('not_subject');

	if(is.not.string(subject))
		return callback('invalid_subject');

	if(!text)
		return callback('not_text');

	if(is.not.string(text))
		return callback('invalid_text');

	if(!html)
		return callback('not_html');

	if(is.not.string(html))
		return callback('invalid_html');

	var data = {
		Destination: {
			ToAddresses: [to]
		},
		Message: {
			Body: {
				Html: {
					Data: html
				},
				Text: {
					Data: text
				}
			},
			Subject: {
				Data: subject
			}
		},
		Source: sender,
		ReplyToAddresses: [sender]
	};

	this.client.sendEmail(data, function (err, data) {
		if(err) return callback(err.toString());
		return callback(null, data.MessageId);
	});
};

module.exports = SES;
