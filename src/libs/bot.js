'use strict';

var SQS = require('aws-sqs-helper');
var SES = require(__dirname + '/ses.js');
var is = require('is_js');
var Base = require(__dirname + '/../base');

function Bot(config) {
	if(is.not.existy(config))
		throw new Error('not_config');

	if(is.not.json(config))
		throw new Error('invalid_config');

	this.sqs = new SQS(config.sqs);

	this.ses = new SES(config.ses);
}

Bot.prototype = Object.create(Base.prototype);

Bot.prototype.run = function(callback){
	var self = this;
	var numberOfMessages = 1;

	this.validateCallback(callback);

	self.sqs.receiveMessage(numberOfMessages, function (err, message){
		if(err)
			return callback({err: err, source: 'SQS_recieveMessage', status:'sqs recieve message failed'}, null, 10000);

		if(is.not.empty(message))
		{
			message = message[0];
			self.ses.send(message.Body.sender, message.Body.to, message.Body.subject, message.Body.text, message.Body.html,
				function(err, response){
					if(err)
						return callback({err: err, source: 'SES_sendMail', status: 'email not sent', message: message.Body}, null, 1000);
					else
					{
						self.sqs.deleteMessage(message.ReceiptHandle, function(err, data){
							if(err)
								return callback({err: err, source: 'SQS_deleteMessage', status: 'sqs message hasn\'t been deleted', message: message}, null, 1000);
						});
						return callback(null, {status: 'email sent', messageId: response, message: message.Body}, 1000);
					}
				});
		}
		else
			return callback(null, {status: 'no data found', message: message}, 10000);
	});
};

module.exports = Bot;