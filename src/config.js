'use strict';

var config = {};

config.development = {
	webport: 4000,
	ses: {
		key: 'Demo',
		secret: 'Demo',
	},
	sqs: {
		accessKey: 'Demo',
		accessToken: 'Demo',
		queue: 'Demo'
	},
	logEntries: '7dd74b90-d82f-40b5-a36b-3835cba2c46e'
};

config.testing = {
	webport: 4100,
	ses: {
		key: 'Demo',
		secret: 'Demo',
	},
	sqs: {
		accessKey: 'Demo',
		accessToken: 'Demo',
		queue: 'Demo'
	},
	logEntries: '7dd74b90-d82f-40b5-a36b-3835cba2c46e'
};

module.exports = config;
