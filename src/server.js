'use strict';

var Logger = require('le_node');
var is = require('is_js');
var environment = process.env.NODE_ENV || 'development';
var config = require(__dirname + '/config.js')[environment];
var Bot = require(__dirname + '/libs/bot.js');

var log = new Logger({
	token: config.logEntries
});

var bot = new Bot(config);

var run = function(){
	bot.run( function(err, data, next_run){
		if(err)
			log.err(err);
		else
			log.info(data);

		if(next_run)
			setTimeout(run, next_run);
	});
};

run();
